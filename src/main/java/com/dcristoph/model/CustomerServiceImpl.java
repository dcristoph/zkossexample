package com.dcristoph.model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class CustomerServiceImpl implements CustomerService {
    private List<Customer> customerList = new ArrayList<>();
    private AtomicInteger id;

    public CustomerServiceImpl(){
        id = new AtomicInteger(1);
        customerList = new ArrayList<>();
        customerList.add( new Customer(
                id.getAndIncrement(),
                "Cristoph",
                "Dziuban",
                "Baker Street 221B, London",
                "Your presenter :-)"
        ));
        customerList.add( new Customer(
                id.getAndIncrement(),
                "Nowhere",
                "Man",
                "Nowhere land",
                "Making all his nowhere plans for nobody"
        ));
    }
    public List<Customer> findAll() {
        return customerList;
    }

    public List<Customer> search(String keyword) {
        List<Customer> result = new ArrayList<>();
        if (keyword==null || "".equals(keyword)){
            result = customerList;
        }else{
            for (Customer c: customerList){
                if (c.getName().toLowerCase().contains(keyword.toLowerCase())
                        ||c.getLastname().toLowerCase().contains(keyword.toLowerCase())){
                    result.add(c);
                }
            }
        }
        return result;
    }
}
