package com.dcristoph.model;

import java.util.List;

public interface CustomerService {
    List<Customer> findAll();
    List<Customer> search(String keyword);
}
