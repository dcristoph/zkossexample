package com.dcristoph.model;

public class Customer {
    private Integer id;
    private String name;
    private String lastname;
    private String address;
    private String description;

    public Customer(Integer id, String name, String lastname, String address, String description){
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.address = address;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
