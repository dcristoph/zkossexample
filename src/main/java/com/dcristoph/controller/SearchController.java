package com.dcristoph.controller;

import com.dcristoph.model.Customer;
import com.dcristoph.model.CustomerService;
import com.dcristoph.model.CustomerServiceImpl;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

import java.util.List;

public class SearchController extends SelectorComposer<Component> {

    @Wire
    private Textbox keywordBox;

    @Wire
    private Listbox customerListbox;

    @Wire
    private Label nameLabel;

    @Wire
    private Label lastnameLabel;

    @Wire
    private Label addressLabel;

    @Wire
    private Label descriptionLabel;

    private CustomerService customerService = new CustomerServiceImpl();

    @Listen("onClick=#searchButton")
    public void search(){
        String keyword = keywordBox.getValue();
        List<Customer> customerList = customerService.search(keyword);
        customerListbox.setModel(new ListModelList<Customer>(customerList));
    }

    @Listen("onSelect=#customerListbox")
    public void showDetail(){
        Customer customer = customerListbox.getSelectedItem().getValue();
        nameLabel.setValue(customer.getName());
        lastnameLabel.setValue(customer.getLastname());
        addressLabel.setValue(customer.getAddress());
        descriptionLabel.setValue(customer.getDescription());
    }

}
