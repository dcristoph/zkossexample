package com.dcristoph.viewModel;

import com.dcristoph.model.Customer;
import com.dcristoph.model.CustomerService;
import com.dcristoph.model.CustomerServiceImpl;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;

import java.util.List;

public class SearchViewModel {

    private String keyword;
    private List<Customer> customerList;
    private Customer selectedCustomer;

    private CustomerService customerService = new CustomerServiceImpl();

    public String getKeyword(){
        return this.keyword;
    }

    public void setKeyword(String keyword){
        this.keyword = keyword;
    }

    public List<Customer> getCustomerList(){
        return this.customerList;
    }

    public Customer getSelectedCustomer(){
        return this.selectedCustomer;
    }

    public void setSelectedCustomer(Customer customer){
        this.selectedCustomer = customer;
    }

    @Command("search")
    @NotifyChange("customerList")
    public void search(){
        customerList = customerService.search(keyword);
    }
}
